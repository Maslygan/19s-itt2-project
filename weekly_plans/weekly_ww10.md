---
Week: 10
Content: Project part 1 phase 2
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 10 Consolidation

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* All groups have recreated another groups minimal system from documentation
* All groups have created issues in other groups project

### Learning goals
* Collaboration and reporting
    * level 1: The student know the content of a good bug report and knwos what merge requests are.
    * level 2: The student is able to file a bug report and make simple merge requests
    * level 3: Bug reporting and merge requests is incorporated into the normal workflow

## Deliverables
* Mandatory weeky meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)

* A working minimal system installed from another groups documentation - see also Exercise 1 in [exercise document](https://eal-itt.gitlab.io/19s-itt2-project/19S_ITT2_exercises.pdf) for details
    * Problems found during install is put into the other groups gitlab issues
    * You will fix whatever issues other groups found in your documentation/scripts
    * You will work primarily through issues on gitlab.

* A report on how the documentation and script were working.

## Schedule

Tuesday

* 8:15 Introdution to the day, general Q/A session

* 8:30 Ulla will talk about study trip

    This includes a short break

* 9:00 MON On reporting bugs and doing merge requests

* 9:30 You work on deliverables

    This includes a group morning meeting
    
    Generic agenda:
    
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.

    Remember to come ask questions if you have any.  

* 10:00 Teacher meetings

    Timeslot for you weekly 10 min. meeting with the teachers.

    Remember to book a time and have an agenda prepared.

* 12:30 - 13:15

    Morten Lassen from career center will talk about the job portal 

Wednesday

* 8:15 Introdution to the day, general Q/A session

* 8:30 Group morning meeting (See monday for agenda)

* 9:00 You work on deliverables.

    Come ask us if you have questions.


## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19s-itt2-project/19S_ITT2_exercises.pdf) for details.

## Comments

* NISI will be absent on Wednesday
* On bug reporting, see [here](https://eal-it-technology.github.io/itt-guides/guides/how-to-file-a-bug-report.html)
* On REST APIs, see [here](https://eal-it-technology.github.io/itt-guides/guides/using-rest-apis.html)