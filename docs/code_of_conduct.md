# ITT-2 project - Code of conduct

# Our Pledge

In the interest of fostering an open and welcoming learning environment, we as students and teachers pledge to making participation in our project and our learning environment a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socio-economic status, nationality, personal appearance, race, religion, political orientation, sexual identity and orientation.

# Practical guidelines

## This makes us feel welcome and included and ensures a welcomin and open learning environment
    
    Be honest and parcitipate in discussions with an open mind.
    Everyone is allowed to have an opinion and talk about it. 
    Jokes are allowed but be aware that your jokes can offend.
    Recognize when someone is talking to you by replying to them.
    Your current conversation is your first priority, do not interrrupt, look at the telephone, computer etc. while conversating.
    Be reliable, honest and admit your mistakes. 
    Have a high skill level and share your knowledge to earn respect.
    Respect each other and their work even if you see problems with it. Let them know in a kind and helping way.
    Help and behave towards people around you like you want to be treated and helped.
    Be empathetic and do your best to focus on personal success and the success of your teammates/classmates.
    Respect that others in the room are concentrating, do not make unnessesary noise etc.     
    Have a constructive attitude.
    Be as clear as possible when communicating.

## This contributes to a negative and unfriendly learning environment? 
    
    Negative contribution by loop hole finding (destructive contribution).
    Being loud.
    Disrespect of given roles.
    Not showing up. 
    Not keeping agreements
    Not being active and engaged.
    Bullying, harrasing and not trying to understand others point of view.

### Examples of behavior that contributes to creating a positive learning environment include

    Using welcoming and inclusive language
    Being respectful of differing viewpoints and experiences
    Gracefully accepting constructive criticism
    Focusing on what is best for the class
    Showing empathy towards other class members

### Examples of unacceptable behavior by participants include

    The use of sexualized language or imagery and unwelcome sexual attention or advances.
    Trolling (intentionally being provocative), insulting/derogatory comments, and personal or political attacks.
    Public or private harassment.
    Publishing others’ private information, such as a physical or electronic address, without explicit permission.
    Other behaviour which could reasonably be considered inappropriate in a learning environment.

# Our Responsibilities

## Common

Everybody is obligated to conform to the rules and standards defined by UCL University College.

## Students

* TBD by class. Use the risk assesment from the pre mortem meeting as a guideline. 

## Teachers

Teachers are responsible for clarifying the standards of acceptable behavior and are expected to take appropriate and fair corrective action in response to any instances of unacceptable behavior.

Teachers provide materials and opportunities for the students to learn the curriculum and other relevant activities in a profesional environment.

Teachers have the right and responsibility to remove, edit, or reject comments, commits, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct.

# Scope

This Code of Conduct applies both within project spaces and in public spaces when an individual is representing the project or its community. Examples of representing a project or community include using an official project e-mail address, posting via an official social media account, or acting as an appointed representative at an online or offline event. Representation of a project may be further defined and clarified by the teachers.

# Enforcement

* TBD by class. How do we handle conflicts?

Instances of abusive, harassing, or otherwise unacceptable behavior may be reported by contacting the teachers at mon@ucl.dk or nisi@ucl.dk. All complaints will be reviewed and investigated and will result in a response that is deemed necessary and appropriate to the circumstances. The project team is obligated to maintain confidentiality with regard to the reporter of an incident. Further, details of specific enforcement policies may be posted separately.