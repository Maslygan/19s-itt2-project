# Hardware components used in 19S-ITT2-project

| Komponent navn     | Link                                     | Varenummer     |
|--------------------|------------------------------------------|----------------|
| DTS62R TACT-SW RØD | https://www.cypax.dk/vare/10.010.6200.20 | 10.010.6200.20 |
| TMP36GT9Z          | https://dk.rs-online.com/web/p/temperatur-fugtighedssensorer/0427351/ | 427-351 |
| 8 kanal I2C Two-Way | https://www.trab.dk/da/breakout/398-8-kanal-i2c-two-way-bidi-logic-level-converter.html | AB492 |