# Exercises for ww10

## Exercise 1 - Recreate from documentation

In this exercise you will recreate another groups minimum system using the documentation that the other group has produced.

If you have problems during the exercise you will report these problems through the other groups gitlab by creating issues with a label named `bug`.

## Instructions

1. Create a `bug` label in your gitlab project.
2. Allow the members from the other group access to create issues in your gitlab project.
3. Hand over your documentation.

    This might be have happended in 2. already, then you need to make it obvious where to find the documentation, e.g. in a top-level readme file.

4. Start recreating another groups minimum system from the documentation you have received.

### While recreating

Make a checklist called `recreation-<other groupname>-<your groupname>.md` in the root of your gitlab project.
The checklist should reflect the minimal system that you are recreating !

The content should be a checklist in this format:

```
Recreation checklist for group <other group name>
===================================================

Group name: <your groupname>

Checklist
------------

- [ ] Project plan completed
- [ ] Minimal circuit schematic completed
- [ ] ATmega328 minimal system code with tempsensor board working
- [ ] ATMega328 to Rasperry Pi serial communication over UART working
- [ ] Raspberry pi configuration week06 requirements fulfilled
- [ ] Networked raspberry pi+atmega system behind a firewall accessible using SSH
- [ ] Temperature readout in celcius from the temperature sensor
- [ ] Raspberry API Server is able to serve temperature readings
- [ ] Virtual server installed
- [ ] `group.md` complete according to specification
- [ ] Dashbaord communicating with API

Comments
-----------

<Any comments that may be relevant for you, other groups or teachers. (Could be that something is super succesful)>


```